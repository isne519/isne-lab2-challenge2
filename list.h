#ifndef LIST
#define LIST

template<class T>
class Node {
public:
	T data;
	Node *next, *prev;
	Node()
	{
		next = prev = 0;
	}
	Node(T el, Node *n = 0, Node *p = 0)
	{
		data = el; next = n; prev = p;
	}
};

template <class T>
class List {
public:
	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	~List();
	void pushToHead(T el);
	void pushToTail(T el);
	T popHead();
	T popTail();
	bool search(T el);
	void print();
private:
	Node<T> * head, *tail;
};

#endif